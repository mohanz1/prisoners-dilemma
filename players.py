import itertools


class Player(object):
    id_iter = itertools.count()

    def __init__(self):
        self.id = next(self.id_iter)
        self.history = []
        self.score = 0
        self.name = None
        self._scores = {
            ("C", "C"): (3, 3),
            ("D", "D"): (1, 1),
            ("C", "D"): (0, 5),
            ("D", "C"): (5, 0),
        }

    def __str__(self):
        return self.name

    def __repr__(self):
        return f"name: {self.name}, id: {self.id}, score: {self.score}, history: {self.history}"

    def strategy(self, opponent):
        return None

    def increase_score(self, points: int):
        self.score += points

    def add_history(self, score, opponent, decision, opponent_decision):
        self.increase_score(score)
        self.history.append(
            {
                "score": score,
                "opponent_id": opponent.id,
                "decision": decision,
                "opponent_decision": opponent_decision,
            }
        )

    def play(self, opponent):
        decision, opponent_decision = self.strategy(opponent), opponent.strategy(self)
        s1, s2 = self.calc_scores(decision, opponent_decision)
        self.add_history(s1, opponent, decision, opponent_decision)
        opponent.add_history(s2, self, opponent_decision, decision)

    def calc_scores(self, decision, opponent_decision) -> (int, int):
        return self._scores[decision, opponent_decision]

    def reset(self):
        self.history, self.score = [], 0


class Defector(Player):
    def __init__(self):
        super().__init__()
        self.name = "Defector"

    def strategy(self, opponent):
        return "D"


class Cooperator(Player):
    def __init__(self):
        super().__init__()
        self.name = "Cooperator"

    def strategy(self, opponent):
        return "C"


class Grudger(Player):
    def __init__(self):
        super().__init__()
        self.name = "Grudger"

    def did_find_defect(self, opponent):
        for game in opponent.history:
            if game["opponent_id"] == self.id and game["decision"] == "D":
                return True
        return False

    def strategy(self, opponent):
        return "D" if self.did_find_defect(opponent) else "C"


class TitForTat(Player):
    def __init__(self):
        super().__init__()
        self.name = "Tit For Tat"

    def find_last_decision(self, opponent):
        for game in reversed(opponent.history):
            if game["opponent_id"] == self.id:
                return game["decision"]
        return None

    def strategy(self, opponent):
        return self.find_last_decision(opponent) or "C"
